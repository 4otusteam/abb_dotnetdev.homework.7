﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Data;
using WebApi.DTOs;
using WebApi.Models;
using System.Data.SQLite;
using System.Data;

namespace WebApi.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DapperContext _context;

        public CustomerRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<long> CreateAsync(CreateCustomerDto createCustomerDTO)
        {
            //CreateCustomerDto ccd = new CreateCustomerDto { customerId = 6, firstName = "newres", lastName = "ult is" };


            CreateCustomerDto ccd = createCustomerDTO;
            string sqlQuery = @"INSERT into Customer (firstName, lastName) values (@firstName, @lastName) returning customerId";
            


            using var connection = _context.CreateConnection();

            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = sqlQuery;
            command.CommandType = CommandType.Text;
            command.Parameters.Add(new SQLiteParameter("@firstName", ccd.firstName));
            command.Parameters.Add(new SQLiteParameter("@lastName", ccd.lastName));
            var customerId = command.ExecuteNonQuery();
            int i = 0;

//            var customerId = await connection.

            return customerId;
        }

        public Task<List<Customer>> GetAllAsync()
        {
            List<Customer> customers = new List<Customer>();

            string sqlQuery = "SELECT * FROM customer";

            var connection = _context.CreateConnection();
            connection.Open();
            var command = connection.CreateCommand();
                command.CommandText = sqlQuery;

                var sqlite_datareader = command.ExecuteReader();
                while (sqlite_datareader.Read())
                {
                
                Customer customer = new Customer
                {
                        Id = (long)sqlite_datareader.GetValue(0),
                        Firstname = sqlite_datareader.GetValue(1).ToString(),//  myreader[1].ToString(),
                        Lastname = sqlite_datareader.GetValue(2).ToString()//  myreader[2].ToString()
                };
                    customers.Add(customer);
                    //                Console.WriteLine(myreader);
                }

            return Task.FromResult(customers);
        }

        public Task<Customer> GetByIdAsync(long id)
        {

            string sqlQuery = $"SELECT * FROM customer where customerId={id}";

            var connection = _context.CreateConnection();
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = sqlQuery;

            var sqlite_datareader = command.ExecuteReader();
            while (sqlite_datareader.Read())
            {

                Customer customer = new Customer
                {
                    Id = (long)sqlite_datareader.GetValue(0),
                    Firstname = sqlite_datareader.GetValue(1).ToString(),//  myreader[1].ToString(),
                    Lastname = sqlite_datareader.GetValue(2).ToString()//  myreader[2].ToString()
                };
                return Task.FromResult(customer);
            }
            return Task.FromResult(new Customer { DisplayError = "User is not found" });


        }
    }
}

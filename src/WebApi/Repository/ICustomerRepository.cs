﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.DTOs;
using WebApi.Models;

namespace WebApi.Repository
{
    public interface ICustomerRepository
    {
        public Task<List<Customer>> GetAllAsync();

        public Task<Customer> GetByIdAsync(long id);

        public Task<long> CreateAsync(CreateCustomerDto createCustomerDTO);
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.DTOs;
using WebApi.Models;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository _cr;
        public CustomerController(ICustomerRepository customerRepository) 
        {
            _cr = customerRepository;
        }


        //public static CustomerRepository getInstance()
        //{
        //    if( _cr == null ) _cr = new CustomerRepository();
        //    return _cr;
        //}

        [HttpGet]
        public Task<List<Customer>> GetCustomersAsync()
        {
            var res = _cr.GetAllAsync();
            return  res;
        } 


        [HttpGet("{id:long}")]   
        
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 404)]
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {

            Customer c = await _cr.GetByIdAsync(id);
            if(c.DisplayError == null)
            {
                return Ok(c);
            }
            else
            {
                return Conflict(c);
            }
        }



        [HttpPost]   
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] CreateCustomerDto customer)
        {
            try
            {

                //long id = GenerateId();
                CreateCustomerDto newCustomer = new CreateCustomerDto{customerId = customer.customerId,   firstName = customer.firstName, lastName = customer.lastName};

                _cr.CreateAsync(newCustomer);
                return new ObjectResult(newCustomer) { StatusCode = 200 };


            }
            catch(Exception ex)
            {
                return BadRequest($"error on adding a customer {ex.Message.ToString()}");
                ///throw new ArgumentException($"error on adding a customer {ex.Message.ToString()}");
            }        
        }

        private long GenerateId()
        {
            long id = 0;
            //var customersId = _cr.customers.Max(x => x.Id);
            //id = customersId+1;
            return id;
        }
    }
}
﻿using Microsoft.Extensions.Configuration;
using System.Data;

using System.Data.SQLite;


namespace WebApi.Data
{
    public class DapperContext
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public DapperContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("SQLiteConnection");
        }

        public IDbConnection CreateConnection()
            => new SQLiteConnection (_connectionString);
    }
}

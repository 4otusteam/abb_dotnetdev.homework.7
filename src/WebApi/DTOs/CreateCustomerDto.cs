﻿namespace WebApi.DTOs
{
    public class CreateCustomerDto
    {
        public long customerId { get; init; }
        
        public string firstName { get; init; }    
        public string lastName { get; init; }
    }
}

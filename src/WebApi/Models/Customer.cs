using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace WebApi.Models
{
    public class Customer
    {
        public long Id { get; init; }

        [Required]
        public string Firstname { get; init; }

        [Required]
        public string Lastname { get; init; }

        #region error
        public string DisplayError { get; set; }
        #endregion

    }

    //public List<Customer> customers { get; set; }




    //public class CustomerRepository
    //{
    //    public IList<Customer> customers { get; init; }
    //    #region error
    //    public string DisplayError { get; set; }
    //    #endregion

    //    public CustomerRepository()
    //    {
    //        customers = new List<Customer>();
            
    //        //if(customers.Count < 1) {
    //            customers.Add(new Customer { Id = 1, Firstname = "User 1", Lastname = "Lasfio 1" });
    //            customers.Add(new Customer { Id = 2, Firstname = "User 2", Lastname = "Lasfio 2" });
    //            customers.Add(new Customer { Id = 3, Firstname = "User 3", Lastname = "Lasfio 3" });
    //        //}
    //    }
    //}
}
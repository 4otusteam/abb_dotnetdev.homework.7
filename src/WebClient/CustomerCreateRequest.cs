namespace WebClient
{
    public class CustomerCreateRequest
    {
        public CustomerCreateRequest()
        {
        }

        public CustomerCreateRequest(
            string firstName,
            string lastName, long? id)
        {
            Firstname = firstName;
            Lastname = lastName;
            Id = id;
        }

        public string Firstname { get; init; }

        public string Lastname { get; init; }
        public long? Id { get; init; }
    }
}
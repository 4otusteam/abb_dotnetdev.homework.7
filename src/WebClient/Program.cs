﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Linq;
using System.Collections.Generic;

namespace WebClient
{
    static class Program
    {
        //static async Task Main(string[] args)
        static void Main(string[] args)
        {

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
            Console.WriteLine("after all");
            //await Task.Run(() =>
            //{
            //    Console.WriteLine("Finish");
            //});

        }

        public static  bool MainMenu()
        {
            var isSuccess = true;
            //Console.TreatControlCAsInput = true;

            //Console.Clear();
            Console.WriteLine("Choose what to do: \n" + 
                $"1 - Search For Existing User \n" +
                $"2 - add new custom user \n" +
                $"3 - show all users   \n" +
                $"4 - Exit \n" 
                );
            String userInput = Console.ReadLine();

            int userId = 0;

            try
            {
                userId = int.Parse(userInput.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Enter only digit");
                MainMenu();
            }

            //           bool isNum = int.TryParse(userInput.ToString(), out userId);
            //bool isNum = , out userId); ;
            Console.WriteLine($"Your choice is: {userId}");
            switch(userId)
            {
                case 1:
                    //Console.WriteLine($"Digit: {userId}");
                    SearchForExistingUser();
                    break;
                case 2:
                    Console.WriteLine("Creating new user:");
                    //CustomerCreateRequest ccr = new CustomerCreateRequest();
                    RandomCustomerAsync();
                    break;

                case 3:
                    ShowAllUsers();
                    break;
                case 4:
                    Console.WriteLine("Bye-bye");
                    Environment.Exit(0);
                    break;
                default:

                    Console.WriteLine("You have entered something wrong");
                    MainMenu();
                    break;
                    ;

            }


            return isSuccess; 
        }

        private static void ShowAllUsers()
        {
            Console.WriteLine("3. Show all users:");
            List<Customer> customers = GetAllCustomers();
            foreach (Customer customer in customers)
            {
                Console.WriteLine($"id: {customer.Id}, firstName: {customer.Firstname}, lastName: {customer.Lastname}");
            }

        }

        private static void SearchForExistingUser()
        {
            Console.Write("Enter userId for a search: ");
            string userInput = Console.ReadLine();
            int userId = 0 ;

            bool isNum = int.TryParse(userInput.ToString(), out userId);
            if(isNum ) 
            {
                GetCustomerInfo(userId);
            }
            else
            {
                Console.WriteLine("You have entered not a digit");
            }
            

        }

        private static async Task<CustomerCreateRequest> RandomCustomerAsync()
        {
            var rand = new Random();

            string firstName = "username_" + rand.Next();
            string lastName = "userlastname_" + rand.Next();
            CustomerCreateRequest ccr = new CustomerCreateRequest( firstName, lastName, null );

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/");
            string parameters = "customers/";
            
            var content = new StringContent(JsonSerializer.Serialize(ccr), System.Text.Encoding.UTF8, "application/json");
               
            var response = client.PostAsync(parameters, content);
            if (response.Status == TaskStatus.WaitingForActivation)
            {
                var contents = await response.Result.Content.ReadAsStringAsync();
                Console.WriteLine($"----New user {contents} is created. ");
            }
          
            return ccr;
        }



        private static Customer GetCustomerInfo(int id)
        {
            Customer customer = new Customer();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/");
            string parameters = "customers/" + id.ToString();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(parameters).Result;
            if(response.IsSuccessStatusCode)
            {
                var dataObject = response.Content.ReadAsStringAsync().Result;

                customer =  JsonSerializer.Deserialize<Customer>(dataObject);
                Console.WriteLine(customer.Firstname);
                Console.WriteLine(customer.Lastname);
                Console.WriteLine(customer.Id);
            }
            else
            {

                Console.WriteLine(($"StatusCode: {response.StatusCode}, {response.ReasonPhrase}"));
            }


            return customer;
        }

        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        private static List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:5001/");
            string parameters = "customers/";
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(parameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var dataObject = response.Content.ReadAsStringAsync().Result;

                customers = JsonSerializer.Deserialize<List<Customer>>(dataObject);
            }
            else
            {

                Console.WriteLine(($"StatusCode: {response.StatusCode}, {response.ReasonPhrase}"));
        
            }


            return customers;
        }

    }
}